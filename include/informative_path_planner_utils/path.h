#ifndef IPP_UTILS_PATH_GEN_H
#define IPP_UTILS_PATH_GEN_H
#include "informative_path_planner_utils/viewpoint_generator.h"
#include "informative_path_planner_utils/viewpoint.h"
#include "semantic_grid/semantic_map_datatypes.h"
namespace ca{
class Path
{
public:
    std::vector<ViewPoint>& global_vp_list_;
    std::vector<size_t> path_;
    double path_length_;
    double path_reward_;
    bool rep_state_initialized_;
    ca::FixedGrid2f* grid2_;
    std::vector<ca::SemanticGridCell> rep_state_;
    bool IsInPath(size_t q, size_t& id);
    double CalculatePathLength(std::vector<size_t>&path, std::vector<ViewPoint>& global_vp_list);
    double DistanceFromPath(std::vector<size_t>&path, std::vector<ViewPoint>& global_vp_list, Eigen::Vector3d& p);
    double DistanceFromLineSegment(Eigen::Vector3d p1, Eigen::Vector3d p2, Eigen::Vector3d p);
    double CostOfAdding(std::vector<size_t>&path, std::vector<ViewPoint>& global_vp_list, Eigen::Vector3d& p);
    bool AddNode(size_t node);
    bool ResetOrder(std::vector<size_t> &new_order);
    void set_path_reward(double path_reward){ path_reward_ = path_reward; }
    double get_path_reward(){return path_reward_;}
    void setRepresentationState(ca::SemanticGridCell *rep_state)
    {
        if(rep_state_.size() > 0)
           rep_state_.clear();

        for(size_t i=0; i < grid2_->dim_i(); i++){
            for(size_t j=0; j < grid2_->dim_j(); j++){
                ca::SemanticGridCell c = rep_state[grid2_->grid_to_mem(i,j)];
                rep_state_.push_back(c);
            }
        }
    }

    Path(std::vector<ViewPoint>& global_vp_list):
        global_vp_list_(global_vp_list)
    {}

    ~Path(){        
        rep_state_.clear();
    }
};
}
#endif
