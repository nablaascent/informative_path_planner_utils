#ifndef IPP_UTILS_REWARD_H
#define IPP_UTILS_REWARD_H
#include "informative_path_planner_utils/path.h"
#include "semantic_grid/shared_semanticmap.hpp"
#include "semantic_grid/semantic_map_datatypes.h"
namespace ca{
class Reward{
public:
 ca::FixedGrid2f* grid2_;
 int max_rays_;// this is 20
 int max_p_; // this is 20
 double exp_base_;// this is 2

 double RewardFunction(size_t new_nodeId,std::vector<ViewPoint>& global_vp_list ,ca::SemanticGridCell *grid_data);
 Reward(){
     max_rays_ = 20;
     max_p_ = 20;
     exp_base_ = 2;
 }
};
}
#endif
