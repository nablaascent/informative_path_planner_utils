#ifndef IPP_UTILS_VIEWPOINT_POINT_H_8547
#define IPP_UTILS_VIEWPOINT_POINT_H_8547
#include <math_utils/math_utils.h>
#include <algorithm>
namespace ca{
enum ViewPointType { Car, ViewCar, Explore };

class ViewPointPose{
public:
    Eigen::Vector3d position_;
    float fov_;
    float heading_;
    float default_fov_;

    //ViewPoint(){ default_fov_ = deg2rad(60);}
};

class ViewPoint{
public:
    ViewPointPose vp_pose_;
    std::vector<size_t> visible_primitives_;
    std::vector<int> num_rays_primitives_;
    //std::vector<float> distance_to_primitives_;
    //std::vector<float> angles_to_primitives_;
    Eigen::Vector3d target_position_;
    size_t target_primitive_;
    bool target_set_;
    bool initialized_;
public:
    ViewPointType vp_type_;
    void AddVisiblePrimitive(size_t id_query);
    void set_target(Eigen::Vector3d &target_position, size_t target_primitive )
        { target_position_ = target_position; target_primitive_ = target_primitive;}
    void get_target(size_t &target_primitive, Eigen::Vector3d &target_position)
        { target_primitive = target_primitive_;
          target_position = target_position_;}
    bool IsVisible(size_t id, int& num_rays, size_t& list_index);
    void get_pose(Eigen::Vector3d& position, float& fov, float &heading)
        {position = vp_pose_.position_;
         fov= vp_pose_.fov_;
         heading = vp_pose_.heading_;
        }
    void set_pose(Eigen::Vector3d& position, float heading, float fov)
        {ResetViewPoint(position,heading,fov);}
    void ResetViewPoint(Eigen::Vector3d& position, float heading, float fov)
        { ClearVisiblePrimitives(); InitializeViewPoint(position,heading,fov);}
    void InitializeViewPoint(Eigen::Vector3d& position, float heading , float fov)
        { ClearVisiblePrimitives();
          vp_pose_.position_ = position;
          vp_pose_.fov_ = fov;
          vp_pose_.heading_ = heading;
        }
    void ClearVisiblePrimitives(){visible_primitives_.clear(); num_rays_primitives_.clear();}
    ViewPoint(std::vector<size_t>& visible_primitives, std::vector<int>& num_rays_primitives)
    {
        visible_primitives_ = visible_primitives;
        num_rays_primitives_ = num_rays_primitives;
    }
    ViewPoint()
    {
        visible_primitives_.clear();
        num_rays_primitives_.clear();
    }
};
}
#endif
