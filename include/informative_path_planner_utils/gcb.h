#ifndef IPP_UTILS_GCB_H
#define IPP_UTILS_GCB_H
#include "informative_path_planner_utils/viewpoint_generator.h"
#include "informative_path_planner_utils/viewpoint.h"
#include "informative_path_planner_utils/path.h"
#include "semantic_grid/semantic_map_datatypes.h"
#include "informative_path_planner_utils/reward.h"
#include "tsp/tsp.h"

namespace ca{
    class GeneralizedCostBenefit
    {
        public:
            std::vector<ViewPoint>& global_vp_list_;
            ViewPointGenerator* vg_;
            Path path_;
            double budget_;
            size_t start_location_id_;
            size_t end_node_id_;
            Path& GCB(Eigen::Vector3d& start_point, double start_heading, double start_fov,
                    size_t end_node_id, ca::SemanticGridCell* grid_data, double budget);
            Reward reward_;
            Eigen::MatrixXd DMatrix(std::vector<size_t> &points, int start_id, int end_id);
            std::vector<size_t> FixOrder(std::vector<size_t> &order, int start_id, int end_id, int fake_id);
            //bool AddNode(ca::ViewPoint &vp);
            // Distance matrix
            // TSP solver
            TSP tsp_;
            void SetupAndSolveTsp(std::vector<size_t> &node_order,size_t start_location_id, size_t end_node_id);
            GeneralizedCostBenefit(std::vector<ViewPoint> & vp_list);
    };
}
#endif
