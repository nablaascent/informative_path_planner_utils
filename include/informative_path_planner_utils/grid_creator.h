#ifndef IPP_UTILS_GRID_CREATOR_H
#define IPP_UTILS_GRID_CREATOR_H
#include "math_utils/math_utils.h"
#include <vector>
namespace ca{
//This creates points in a grid
    bool GetGrid(std::vector<Eigen::Vector2d> &grid_points, Eigen::Vector2d p1, Eigen::Vector2d p2, Eigen::Vector2d p3, double x_res, double y_res);
}
#endif
