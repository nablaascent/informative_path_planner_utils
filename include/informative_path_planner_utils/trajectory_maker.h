#ifndef IPP_UTILS_TRAJECTORY_MAKER_H
#define IPP_UTILS_TRAJECTORY_MAKER_H
#include "informative_path_planner_utils/viewpoint_generator.h"
#include "informative_path_planner_utils/viewpoint.h"
#include "semantic_grid/semantic_map_datatypes.h"
#include "informative_path_planner_utils/path.h"
#include "math_utils/math_utils.h"
#include "ca_nav_msgs/PathXYZVViewPoint.h"
// in case the vehicle is trying to look at the target
// -- Let it come to the thing and give it some time to come to terms with it
namespace ca{
class TrajectoryGenerator
{
public:
    double high_velocity_;
    double low_velocity_;
    double vehicle_fov_;
    double time_bet_frames_;
    ca_nav_msgs::PathXYZVViewPoint GetTrajectory(Eigen::Vector3d current_pos, double heading, double curr_velocity ,Path &path, std::string frame);
    std::vector<double> TimeTrajectory( Eigen::Vector3d current_pos, double curr_heading, double curr_velocity, Path &path);
    bool GetState(double time, Path &path, Eigen::Vector3d current_pos, double curr_heading, std::vector<double> &time_path, Eigen::Vector3d &ret_pos, double &ret_heading);
    bool GetPos(Eigen::Vector3d& pos1, Eigen::Vector3d &pos2, double time1, double time2, double q_time, Eigen::Vector3d &ret_pos);
    bool GetHeading(double heading1, double fov1, double c_heading1, double heading2,
                    double fov2, double c_heading2, double time1, double time2, double q_time, double &ret_heading);
    double GetVpTime(ca::ViewPoint& vp);
    double GetVpDistance(ca::ViewPoint& vp);
    ca_nav_msgs::PathXYZVViewPoint MakeTrajectory(Eigen::Vector3d current_pos, double heading, Path &path, std::string frame);
    std::vector<ca_nav_msgs::XYZVViewPoint> GetViewPontWaypoints(Eigen::Vector3d incoming_dir,Eigen::Vector3d outgoing_dir,
                                                                 ca::ViewPoint &vp);

};
}
#endif
