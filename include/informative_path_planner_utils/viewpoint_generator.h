#ifndef IPP_UTILS_VIEWPOINT_GEN_H
#define IPP_UTILS_VIEWPOINT_GEN_H
#include "semantic_grid/semantic_map_datatypes.h"
#include "semantic_grid/shared_semanticmap.hpp"
#include "informative_path_planner_utils/viewpoint.h"
#include "boost/bind.hpp"
#include "semantic_grid/camera_interface.h"


// heading can be set by this as well, will do this later, ye batayega ki node heading kahan hai

//Path sirf viewpoints pe operate karega, it will have viewpoint list and a pointer to changed mem
namespace ca{
class ViewPointGenerator{
public:
    ca::FixedGrid2f* grid2_;
    ca::SemanticGridCell* grid_data_;
    std::vector<size_t> return_list_;
    float angle_res_;
    float height_threshold_;
    std::vector<Eigen::Vector3d> cached_rays_;


    bool initialized_;
    bool CountCell(float height_threshold,
                   int x, int y, int z, int s_x, int s_y, int s_z);


    std::vector<size_t> NumIntersectingRays(Eigen::Vector3d& pos, Eigen::Vector3d dir, float height_threshold);

    void MakeViewPoint(ca::ViewPoint &vp ,Eigen::Vector3d &pos, float heading, float fov);
    ViewPointGenerator(){
        initialized_ = false;
    }

    bool Initialize(ca::SharedSemanticMap &sgmap, LabelTensorInterface &cmi, tf::Transform& transform, float angle_res);
};
}
#endif
