#ifndef IPP_WRAPPER_GCB_H
#define IPP_WRAPPER_GCB_H
#include "informative_path_planner_utils/viewpoint_generator.h"
#include "informative_path_planner_utils/gcb.h"
#include "informative_path_planner_utils/grid_creator.h"
#include "visualization_msgs/MarkerArray.h"
#include "local_frame_manager/local_frame_manager.h"
#include "informative_path_planner_utils/trajectory_maker.h"
#include "ros/ros.h"

namespace ca{
    class IPPWrapper
    {
        public:
            std::vector<ViewPoint>& global_vp_list_;
            GeneralizedCostBenefit gcb_;
            ViewPointGenerator vg_;
            ca::FixedGrid2f* grid2_;
            ca::SemanticGridCell* grid_data_;
            std::string grid_frame_;
            LocalFrameManager lfm_client_;
            Eigen::Vector2d p_ori_;
            Eigen::Vector2d p_x_;
            Eigen::Vector2d p_y_;
            double grid_height_;
            double x_res_;
            double y_res_;
            void RewardTesting();
            TrajectoryGenerator tg_;
            ca_nav_msgs::PathXYZVViewPoint published_traj_;
            double budget_;
            bool Replan(Eigen::Vector3d loookahead_pose,double &budget);
            double checkoff_distance_;
            double replan_distance_;
            Eigen::Vector3d prev_lookahead_;
            bool prev_lookahead_initialized_;
            visualization_msgs::MarkerArray Displaytrajectory(ca_nav_msgs::PathXYZVViewPoint& traj);
            IPPWrapper(ca::FixedGrid2f* grid2, ca::SemanticGridCell* grid_data,std::vector<ViewPoint> &vp, std::string grid_frame):
            gcb_(vp),
            global_vp_list_(vp)
            {
                grid_frame_ = grid_frame;
                grid2_ = grid2;
                grid_data_ = grid_data;
                prev_lookahead_initialized_ = false;
            }
            void Initialize(ca::SharedSemanticMap &sgmap, LabelTensorInterface &cmi, tf::Transform& transform,
                            float angle_res, ros::NodeHandle &n);
            void generate_global_vp_list();
            visualization_msgs::MarkerArray GetPathMarker(uint8_t mode);
            void CleanUpVPList();

    };
}
#endif
