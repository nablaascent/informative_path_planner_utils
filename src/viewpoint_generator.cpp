#include "informative_path_planner_utils/viewpoint_generator.h"

//    Where to place the nodes

bool ca::ViewPointGenerator::CountCell(float height_threshold,
               int x, int y, int z, int s_x,int s_y, int s_z){
    //std::vector<size_t> return_list;
    bool cont_raycasting = true;
    if(!grid2_->is_inside_grid(x,y))    return false;
    //ROS_INFO("It is inside grid");
    ca::mem_ix_t mem_ix = grid2_->grid_to_mem(x, y);

    ca::SemanticGridCell& old_value = grid_data_[mem_ix];
    double height = z;

    if((old_value.height_mean_ - height_threshold) <= height){
        return_list_.push_back(mem_ix);
    }

    if((old_value.height_mean_ <= height))
        cont_raycasting = false;

    //float distance = std::sqrt(std::pow(s_x-x,2)*grid2_->resolution() + std::pow(s_y-y,2)*grid2_->resolution() + std::pow(s_z-z,2));

    return cont_raycasting;
}


std::vector<size_t> ca::ViewPointGenerator::NumIntersectingRays(Eigen::Vector3d& pos, Eigen::Vector3d dir, float height_threshold){
    //return_list_.clear();
    double range = 60.0;
     ca::FixedGrid2f::Vec2 p(pos.x(),pos.y());
     if(grid2_->is_inside_box(p)){
         ca::Vec2Ix sensor_pos_ij = grid2_->world_to_grid(p);
         dir = pos+range*dir;
         ca::FixedGrid2f::Vec2 r(dir.x(),dir.y());
         ca::Vec2Ix ray_end_pos_ij =  grid2_->world_to_grid(r);
         ca::Vec3Ix sensor_pos_ijk;
         sensor_pos_ijk.x() = sensor_pos_ij.x(); sensor_pos_ijk.y() = sensor_pos_ij.y();
         sensor_pos_ijk.z() = round(pos.z());

         ca::Vec3Ix ray_end_ijk;
         ray_end_ijk.x() = ray_end_pos_ij.x(); ray_end_ijk.y() = ray_end_pos_ij.y();
         ray_end_ijk.z() = round(dir.z());

         boost::function<bool (int,int,int)> UpCell( boost::bind(&ca::ViewPointGenerator::CountCell ,this ,height_threshold,
                                                                 _1,_2,_3,
                                                                  sensor_pos_ijk.x(), sensor_pos_ijk.y(),sensor_pos_ijk.z()) );
         ca::bresenham_trace(sensor_pos_ijk,ray_end_ijk,UpCell);
     }
     return return_list_;
}


bool ca::ViewPointGenerator::Initialize(ca::SharedSemanticMap &sgmap, LabelTensorInterface &cmi, tf::Transform& transform, float angle_res){
    // The transform is from camera to /camera to dji_odometry
    angle_res_ = angle_res;
    if(!cmi.Initialized())
        return false;

    cached_rays_.clear();
    grid2_ = sgmap.GetFixedGrid();
    grid_data_ = sgmap.GetMem();


    for(size_t i=0; i<cmi.labels_data_layout_.dim[1].size;i+=2){
        for(size_t j=0; j<cmi.labels_data_layout_.dim[2].size;j+=2){
            Eigen::Vector3d vv = cmi.getRayAt(i,j); std::swap(vv.x(),vv.y());
            ca::tf_utils::transformVector3d(transform,vv,true);
            cached_rays_.push_back(vv);
        }
    }
    initialized_ = true;
    return true;
}


void ca::ViewPointGenerator::MakeViewPoint(ca::ViewPoint &vp ,Eigen::Vector3d &pos, float heading, float fov){
    vp.ResetViewPoint(pos,heading,fov);

    float heading_start = heading - 0.5*fov;
    float heading_ending = heading + 0.5*fov;
    Eigen::Vector3d vv;
    Eigen::Quaterniond  q(0.0,0.0,0.0,0.0);
    double qa;

    //ROS_INFO_STREAM("Making ViewPoint::"<<heading_start<<"::"<<heading_ending<<"Cached Rays::"<<cached_rays_.size());

    for(float heading_loop = heading_start; heading_loop < heading_ending; heading_loop += angle_res_){
        // Something is wrong in number of times this for loop is run
        for(size_t i=0; i<cached_rays_.size(); i++){            
            vv = cached_rays_[i];
            if(vv.z()>0.0){
                qa = 0.5*heading_loop;
                q.w() = std::cos(qa); q.z() = std::sin(qa);
                vv = q*vv;
                NumIntersectingRays(pos, vv, height_threshold_);
                for(size_t j=0; j<return_list_.size();j++){
                    vp.AddVisiblePrimitive(return_list_[j]);
                }
            }
            return_list_.clear();
        }

    }
    //ROS_INFO_STREAM("Made viewpoint::"<<vp.visible_primitives_.size());
}
