#include "informative_path_planner_utils/viewpoint.h"

bool ca::ViewPoint::IsVisible(size_t id, int& num_rays, size_t& list_index){
    std::vector<size_t>::iterator it;

    it = std::find (visible_primitives_.begin(), visible_primitives_.end(), id);
    if (it == visible_primitives_.end())
        return false;

    list_index = it - visible_primitives_.begin();
    num_rays = num_rays_primitives_[list_index];
    return true;
}

void ca::ViewPoint::AddVisiblePrimitive(size_t id_query)
{
    int num_rays=1; size_t lid;
    if(!IsVisible(id_query,num_rays,lid)){
        visible_primitives_.push_back(id_query);
        num_rays_primitives_.push_back(num_rays);
    }
    else
        num_rays_primitives_[lid] = num_rays + 1;
};
