#include "informative_path_planner_utils/trajectory_maker.h"


ca_nav_msgs::PathXYZVViewPoint ca::TrajectoryGenerator::GetTrajectory(Eigen::Vector3d current_pos, double heading, double curr_velocity, Path &path, std::string frame){

    ca_nav_msgs::PathXYZVViewPoint traj;
    traj.header.frame_id = frame;
    traj.header.stamp = ros::Time::now();


    if(path.path_.size() == 0)
        return traj;

    std::vector<double> time_path = ca::TrajectoryGenerator::TimeTrajectory(current_pos, heading, curr_velocity, path);
    for(int i=1; i<time_path.size();i++){
        ca::ViewPoint &vp1 = path.global_vp_list_[path.path_[i-1]];
        size_t next_id = i;
        if(i==(time_path.size()-1))
            next_id = i-1;

        ca::ViewPoint &vp2 = path.global_vp_list_[path.path_[next_id]];

        double start_time = time_path[i] - time_bet_frames_ - (0.5*vp1.vp_pose_.fov_/vehicle_fov_)*time_bet_frames_;
        if(start_time < time_path[i-1])
            start_time = time_path[i-1];

        double end_time = time_path[i] + (0.5*vp1.vp_pose_.fov_/vehicle_fov_)*time_bet_frames_;

        for(double t=start_time; t<=end_time;t = t+time_bet_frames_){
            Eigen::Vector3d ret_pos;
            double ret_heading;
            if(GetState(t, path, current_pos,heading,time_path,ret_pos,ret_heading)){
                ca_nav_msgs::XYZVViewPoint vp_msg;
                vp_msg.position.x = ret_pos.x();
                vp_msg.position.y = ret_pos.y();
                vp_msg.position.z = ret_pos.z();
                vp_msg.safety = false;
                vp_msg.vel = high_velocity_;
                if(vp2.target_set_ && vp1.target_set_)
                    vp_msg.vel = low_velocity_;

                Eigen::Vector3d vp_vec = ret_pos +
                                        5*Eigen::Vector3d(std::cos(ret_heading),std::sin(ret_heading),0.0); //TODO change the extension later
                vp_msg.viewpoint.x = vp_vec.x();
                vp_msg.viewpoint.y = vp_vec.y();
                vp_msg.viewpoint.z = vp_vec.z();

                if(t==start_time){
                    vp_msg.safety = true;
                }
                std::cout<<"Ret pos::"<<ret_pos.x()<<"::"<<ret_pos.y()<<"::"<<ret_pos.z()<<" safety::"<<(int)vp_msg.safety<<" Heading::"<<ret_heading<<"\n";
                traj.waypoints.push_back(vp_msg);
            }            
        }
        std::cout<<"----------------New Path wp-----------\n";

    }
    return traj;
}


bool ca::TrajectoryGenerator::GetState(double qtime, Path &path, Eigen::Vector3d current_pos,
                                       double curr_heading, std::vector<double> &time_path,Eigen::Vector3d& ret_pos,double& ret_heading){
    size_t index_prev=0;
    size_t index_next=0;
    for(int i=1;i<time_path.size();i++){
        if(time_path[i] > qtime){
            index_prev = i-1;
            index_next = i;
            break;
        }
    }
    //std::cout<<"Qtime::"<<qtime<<" Index Prev::"<<index_prev<<"\n Index Next::"<<index_next<<"\n";
    if(index_prev == index_next && index_next == 0)
        return false;

    Eigen::Vector3d pos1;
    Eigen::Vector3d pos2;

    if(index_prev==0)
        pos1 = current_pos;
    else
        pos1 = path.global_vp_list_[path.path_[index_prev-1]].vp_pose_.position_;

    pos2 = path.global_vp_list_[path.path_[index_next-1]].vp_pose_.position_;


    if(!GetPos(pos1, pos2, time_path[index_prev], time_path[index_next], qtime, ret_pos)){
        std::cout<<"Didnt get a pose. \n";
        return false;
    }



    double c_heading1 = 0.0;
    Eigen::Vector3d vec = pos2 -pos1;
    double c_heading2 =std::atan2(vec.y(),vec.x());    


    double heading1 = curr_heading;
    double fov1 = vehicle_fov_;
    if(index_prev>0){
        ca::ViewPoint &vp1 = path.global_vp_list_[path.path_[index_prev-1]];
        heading1 = vp1.vp_pose_.heading_;
        fov1 = vp1.vp_pose_.fov_;
    }

    ca::ViewPoint &vp2 = path.global_vp_list_[path.path_[index_next-1]];
    double heading2 = vp2.vp_pose_.heading_;
    double fov2 = vp2.vp_pose_.fov_;

    ca::TrajectoryGenerator::GetHeading(heading1, fov1, c_heading1, heading2,
                    fov2, c_heading2, time_path[index_prev], time_path[index_next],
                    qtime, ret_heading);
    return true;
}

std::vector<double> ca::TrajectoryGenerator::TimeTrajectory( Eigen::Vector3d current_pos, double curr_heading, double curr_velocity, ca::Path &path){
    double current_time = 0.0;
    std::vector<double> trajectoryTime;
    trajectoryTime.push_back(current_time);
    for(size_t i=0; i < path.path_.size(); i++){
        ca::ViewPoint& vp = path.global_vp_list_[path.path_[i-1]]; // We can calculate its time, I mean woth heading and all
        Eigen::Vector3d prev_pos = current_pos;
        double velocity = curr_velocity;
        if(i>0){
            prev_pos = vp.vp_pose_.position_;
            if(!vp.target_set_)
                velocity = high_velocity_;
            else
                velocity = low_velocity_;

        }
        Eigen::Vector3d next_pos = path.global_vp_list_[path.path_[i]].vp_pose_.position_;
        double distance = (next_pos - prev_pos).norm();
        current_time = current_time + distance/velocity;
        trajectoryTime.push_back(current_time);
    }
    return trajectoryTime;
}


bool ca::TrajectoryGenerator::GetPos(Eigen::Vector3d& pos1, Eigen::Vector3d &pos2, double time1, double time2, double q_time, Eigen::Vector3d &ret_pos){
    //std::cout<<"The times time1::"<<time1<<" qtime::"<<q_time<<" time2::"<<time2;
    if(q_time < time1 || q_time > time2){
        return false;
    }
    double k = (q_time - time1)/(time2 - time1);
    ret_pos = (1-k)*pos1 + k*pos2;
    return true;
}

bool ca::TrajectoryGenerator::GetHeading(double heading1, double fov1, double c_heading1, double heading2,
                double fov2, double c_heading2, double time1, double time2,
                double q_time, double &ret_heading){
    if(q_time < time1 || q_time > time2){
        return false;
    }

    int num_wp1 = std::ceil(fov1/vehicle_fov_);
    int num_wp2 = std::ceil(fov2/vehicle_fov_);
    double time_start_spinning1 = time1 - num_wp1*0.5*time_bet_frames_;
    double time_end_spinning1 = time1 + num_wp1*0.5*time_bet_frames_;

    double time_start_spinning2 = time2 - num_wp2*0.5*time_bet_frames_;
    double time_end_spinning2 = time2 + num_wp2*0.5*time_bet_frames_;

    if(q_time >= time_start_spinning1 && q_time <= time_end_spinning1){
        double min_heading = heading1 - 0.5*fov1;
        double max_heading = heading1 + 0.5*fov1;
        double k = (q_time - time_start_spinning1)/
                (time_end_spinning1 - time_start_spinning1);

        ret_heading = (1-k)*min_heading + k*max_heading;
        //std::cout<<"\n1 Min heading::"<<min_heading<<" Max heading::"<<max_heading<<"Ret heading::"<<ret_heading;

    }
    else if(q_time >= time_start_spinning2 && q_time <= time_end_spinning2){
        double min_heading = heading2 - 0.5*fov2;
        double max_heading = heading2 + 0.5*fov2;
        double k = (q_time - time_start_spinning2)/
                (time_end_spinning2 - time_start_spinning2);

        ret_heading = (1-k)*min_heading + k*max_heading;
        //std::cout<<"\n2 Min heading::"<<min_heading<<" Max heading::"<<max_heading<<"Ret heading::"<<ret_heading;

    }
    else{
        ret_heading = c_heading2;
        //std::cout<<"Ret heading::"<<ret_heading;
    }

    //std::cout<<"\n t1s::"<<time_start_spinning1<<" t1e::"<<time_end_spinning1<<" t2s::"<<time_start_spinning2<<" t2e::"<<time_end_spinning2<<" q_time::"<<q_time<<"\n";

    return true;
}


double ca::TrajectoryGenerator::GetVpTime(ca::ViewPoint& vp){
    // Not supporting multiple mode for now, lets only do explore
    return (vp.vp_pose_.fov_/vehicle_fov_ + time_bet_frames_);

}

double ca::TrajectoryGenerator::GetVpDistance(ca::ViewPoint& vp){
    double vp_time = (vp.vp_pose_.fov_/vehicle_fov_ + time_bet_frames_);
    return (vp_time*low_velocity_);
}

ca_nav_msgs::PathXYZVViewPoint ca::TrajectoryGenerator::MakeTrajectory(Eigen::Vector3d current_pos, double heading,
                                                                       Path &path, std::string frame){
    ca_nav_msgs::PathXYZVViewPoint traj;
    traj.header.stamp = ros::Time::now();
    traj.header.frame_id = frame;
    for(size_t i=0; i<path.path_.size();i++){
        Eigen::Vector3d prev_pos = current_pos;
        ca::ViewPoint& vp_next = path.global_vp_list_[path.path_[i]];
        if(i>0){
            ca::ViewPoint& vp_prev = path.global_vp_list_[path.path_[i-1]];
            prev_pos = vp_prev.vp_pose_.position_;
        }

        Eigen::Vector3d incoming_dir = vp_next.vp_pose_.position_-prev_pos;
        incoming_dir.normalize();
        Eigen::Vector3d outgoing_dir = incoming_dir;
        if(i!=(path.path_.size()-1)){
            ca::ViewPoint& vp_agla = path.global_vp_list_[path.path_[i+1]];
            outgoing_dir = vp_agla.vp_pose_.position_ - vp_next.vp_pose_.position_;
            outgoing_dir.normalize();
        }
        std::vector<ca_nav_msgs::XYZVViewPoint> wp_list=
                                GetViewPontWaypoints(incoming_dir,outgoing_dir,vp_next);
        for(int i=0; i<wp_list.size();i++){
            traj.waypoints.push_back(wp_list[i]);
        }
    }

    return traj;
}

std::vector<ca_nav_msgs::XYZVViewPoint> ca::TrajectoryGenerator::
                                        GetViewPontWaypoints(Eigen::Vector3d incoming_dir,Eigen::Vector3d outgoing_dir,
                                                             ca::ViewPoint &vp){
    std::vector<ca_nav_msgs::XYZVViewPoint> wp_list;
    ca_nav_msgs::XYZVViewPoint wp;
    double start_time = -((0.5*vp.vp_pose_.fov_/vehicle_fov_)*time_bet_frames_ + time_bet_frames_);
    double end_time = (0.5*vp.vp_pose_.fov_/vehicle_fov_)*time_bet_frames_;

    Eigen::Vector3d pos = start_time*low_velocity_*incoming_dir + vp.vp_pose_.position_;
    double velocity = high_velocity_;

    wp.position.x = pos.x(); wp.position.y = pos.y(); wp.position.z = pos.z();
    wp.vel = high_velocity_;
    wp.safety = true;
    wp_list.push_back(wp);

    double min_heading = vp.vp_pose_.heading_ - 0.5*vp.vp_pose_.fov_;
    double max_heading = vp.vp_pose_.heading_ + 0.5*vp.vp_pose_.fov_;

    for(double heading = min_heading; heading <max_heading; heading = heading + vehicle_fov_){
        heading = std::min(max_heading,heading);
        double heading_time = time_bet_frames_*((heading-vp.vp_pose_.heading_)/vehicle_fov_);
        if(heading_time<0){
            pos = heading_time*low_velocity_*incoming_dir + vp.vp_pose_.position_;
        }
        else{
            pos = heading_time*low_velocity_*outgoing_dir + vp.vp_pose_.position_;
        }
        wp.vel = low_velocity_;
        wp.safety = false;
        wp.position.x = pos.x(); wp.position.y = pos.y(); wp.position.z = pos.z();
        wp.viewpoint.x = pos.x()+5*std::cos(heading); wp.viewpoint.y = pos.y()+5*std::sin(heading); wp.viewpoint.z = pos.z();
        wp_list.push_back(wp);
    }

    return wp_list;

}
