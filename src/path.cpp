#include "informative_path_planner_utils/path.h"

bool ca::Path::IsInPath(size_t q, size_t& id){
    std::vector<size_t>::iterator it;

    it = std::find (path_.begin(), path_.end(), q);
    if (it == path_.end())
        return false;

    id = it - path_.begin();
    return true;
}

double ca::Path::CalculatePathLength(std::vector<size_t>&path, std::vector<ViewPoint>& global_vp_list){
    path_length_ = 0;
    if(path_.size() < 2 )
        return 0;

    for(size_t i=0; i<path.size()-1;i++){
       Eigen::Vector3d& v1 = global_vp_list[path[i]].vp_pose_.position_;
       Eigen::Vector3d& v2 = global_vp_list[path[i+1]].vp_pose_.position_;
       Eigen::Vector3d dd = v1-v2;
       path_length_ += path_length_ + dd.norm();
    }

    return path_length_;

}

bool ca::Path::AddNode(size_t node){
    size_t dummy;
    if(IsInPath(node,dummy))
        return false;

    ca::ViewPoint& vp = global_vp_list_[node];
    path_.push_back(node);
    //This can be quickened by reserving
    for(size_t i=0; i<vp.visible_primitives_.size();i++){
        ca::SemanticGridCell& sc =rep_state_[vp.visible_primitives_[i]];
        int log_odds = sc.integrated_logodds_[(size_t)ca::SemanticClass::car];

        if(log_odds<0)
            log_odds = log_odds - vp.num_rays_primitives_[i]; // Most likely observation
        else
            log_odds = log_odds + vp.num_rays_primitives_[i]; // Most likely observation

        sc.integrated_logodds_[(size_t)ca::SemanticClass::car] = log_odds;
    }
    return true;
}

bool ca::Path::ResetOrder(std::vector<size_t> &new_order){
    std::vector<size_t> new_path = path_;
    //std::cout<<"\n New Order::";
    for(size_t i=0; i<new_order.size(); i++){
        new_path[i] = path_[new_order[i]];
        //std::cout<<new_path[i]<<"-->";
    }
    //std::cout<<"\n";
    path_ = new_path;
    return true;
}

double ca::Path::DistanceFromPath(std::vector<size_t>&path, std::vector<ViewPoint>& global_vp_list, Eigen::Vector3d& p){
    if(path_.size() ==0 )
        return 0;
    Eigen::Vector3d pp = p;
    if(path_.size() == 1){
        Eigen::Vector3d v =
                global_vp_list[path[0]].vp_pose_.position_ - pp;
        return v.norm();
    }


    double d = 1000000.0;
    for(size_t i=0;i<path_.size()-1;i++){
        Eigen::Vector3d v1 =
                global_vp_list[path[i]].vp_pose_.position_;
        Eigen::Vector3d v2 =
                global_vp_list[path[i+1]].vp_pose_.position_;
        d= std::min(d,DistanceFromLineSegment(v1,v2,pp));
    }

    return d;
}

double ca::Path::DistanceFromLineSegment(Eigen::Vector3d p1, Eigen::Vector3d p2, Eigen::Vector3d p){

        Eigen::Vector3d a = p2-p1;
        Eigen::Vector3d b = p-p1;

        double d = (a.cross(b)).norm()/a.norm();

        Eigen::Vector3d aa = p1-p2;
        Eigen::Vector3d bb = p-p2;

        double d1 = b.norm();
        double d2 = bb.norm();

        if(a.dot(b) <0 || aa.dot(bb)<0) // Checking for obtuse angles
            d = std::min(d1,d2);

        return d;
}

double ca::Path::CostOfAdding(std::vector<size_t>&path, std::vector<ca::ViewPoint>& global_vp_list, Eigen::Vector3d& p){
    return 2*DistanceFromPath(path, global_vp_list, p);
}
