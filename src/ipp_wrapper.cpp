#include "informative_path_planner_utils/ipp_wrapper.h"

void ca::IPPWrapper::Initialize(ca::SharedSemanticMap &sgmap, LabelTensorInterface &cmi, tf::Transform& transform,
                                float angle_res, ros::NodeHandle& n){
    vg_.Initialize(sgmap,cmi,transform,angle_res);
    vg_.height_threshold_ = 1.5;

    gcb_.vg_ = &vg_;
    gcb_.reward_.grid2_ = sgmap.GetFixedGrid();
    gcb_.reward_.max_rays_ = 30;
    gcb_.path_.grid2_ = sgmap.GetFixedGrid();

    bool server = false;
    if(!lfm_client_.Initialize(server,n)){
        ROS_ERROR_STREAM("Could not initialize local frame manager server.");
        exit(-1);
    }

    std::string param = "exploration_grid/origin/lat";
    if (!n.getParam(param, p_ori_.x())){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "exploration_grid/origin/lon";
    if (!n.getParam(param, p_ori_.y())){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "exploration_grid/x_lat";
    if (!n.getParam(param, p_x_.x())){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "exploration_grid/x_lon";
    if (!n.getParam(param, p_x_.y())){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "exploration_grid/y_lat";
    if (!n.getParam(param, p_y_.x())){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "exploration_grid/y_lon";
    if (!n.getParam(param, p_y_.y())){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    double grid_height;
    param = "exploration_grid/grid_height";
    if (!n.getParam(param, grid_height)){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "exploration_grid/x_res";
    if (!n.getParam(param, x_res_)){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "exploration_grid/y_res";
    if (!n.getParam(param, y_res_)){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "path_generation/high_vel";
    if (!n.getParam(param, tg_.high_velocity_)){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "path_generation/low_vel";
    if (!n.getParam(param, tg_.low_velocity_)){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "path_generation/vehicle_fov";
    if (!n.getParam(param, tg_.vehicle_fov_)){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }


    param = "path_generation/time_bet_frames";
    if (!n.getParam(param, tg_.time_bet_frames_)){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "path_generation/checkoff_distance";
    if (!n.getParam(param, checkoff_distance_)){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "path_generation/replan_distance";
    if (!n.getParam(param, replan_distance_)){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }

    param = "budget";
    if (!n.getParam(param, budget_)){
      ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
      exit(-1);
    }


    Eigen::Vector3d v;
    ros::Rate loop_rate(0.5);
    while(!lfm_client_.TransformLatLon2LocalFrame(p_ori_.x()*DEG2RAD_C,p_ori_.y()*DEG2RAD_C,0.0,v,grid_frame_)){
        ROS_ERROR_STREAM("Couldn't transform lat lon to "<<grid_frame_);
        ros::spinOnce();
        loop_rate.sleep();
    }
    p_ori_.x() = v.x(); p_ori_.y() = v.y();

    while(!lfm_client_.TransformLatLon2LocalFrame(p_x_.x()*DEG2RAD_C,p_x_.y()*DEG2RAD_C,0.0,v,grid_frame_)){
        ROS_ERROR_STREAM("Couldn't transform lat lon to "<<grid_frame_);
        ros::spinOnce();
        loop_rate.sleep();
    }
    p_x_.x() = v.x(); p_x_.y() = v.y();

    while(!lfm_client_.TransformLatLon2LocalFrame(p_y_.x()*DEG2RAD_C,p_y_.y()*DEG2RAD_C,0.0,v,grid_frame_)){
        ROS_ERROR_STREAM("Couldn't transform lat lon to "<<grid_frame_);
        ros::spinOnce();
        loop_rate.sleep();
    }
    p_y_.x() = v.x(); p_y_.y() = v.y();

    grid_height_ = grid_height;

}


visualization_msgs::MarkerArray ca::IPPWrapper::GetPathMarker(uint8_t mode){
    visualization_msgs::MarkerArray ma;
    visualization_msgs::Marker m;
    m.header.frame_id = grid_frame_;
    m.header.stamp = ros::Time();
    m.ns = "path";
    m.frame_locked = true;
    m.id = 0;
    m.type = visualization_msgs::Marker::LINE_LIST;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = 0.0;
    m.pose.position.y = 0.0;
    m.pose.position.z = 0.0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 1;
    m.scale.y = 1;
    m.scale.z = 1;
    m.color.a = 1.0; // Don't forget to set the alpha!
    m.color.r = 0.2;
    m.color.g = 0.8;
    m.color.b = 0.1;
    m.points.clear();

    geometry_msgs::Point p;

    ROS_INFO_STREAM(" Path::");
    for(size_t i=0; i<gcb_.path_.path_.size()-1;i++){
        {
            ca::ViewPoint& vp =  global_vp_list_[gcb_.path_.path_[i]];
            Eigen::Vector3d & v = vp.vp_pose_.position_;
            ROS_INFO_STREAM(gcb_.path_.path_[i]<<"::"<<v.x()<<"::"<<v.y()<<"::"<<v.z()<<"->");
            p.x = v.x(); p.y = v.y(); p.z = v.z();
            m.points.push_back(p);
        }

        {
            ca::ViewPoint& vp =  global_vp_list_[gcb_.path_.path_[i+1]];
            Eigen::Vector3d & v = vp.vp_pose_.position_;
            ROS_INFO_STREAM(gcb_.path_.path_[i+1]<<"::"<<v.x()<<"::"<<v.y()<<"::"<<v.z()<<"\n");
            p.x = v.x(); p.y = v.y(); p.z = v.z();
            m.points.push_back(p);
        }
    }

    ma.markers.push_back(m);
    m.points.clear();
    m.ns = "viewpoints";
    m.type = visualization_msgs::Marker::SPHERE_LIST;
    m.scale.x = 2;
    m.scale.y = 2;
    m.scale.z = 2;
    m.color.a = 0.5; // Don't forget to set the alpha!
    m.color.r = 0.1;
    m.color.g = 0.1;
    m.color.b = 0.9;
    std_msgs::ColorRGBA cc;
    cc.r = 1.0;cc.g = 0; cc.b = 0; cc.a =1;
    for(size_t i=0; i<global_vp_list_.size();i++){
        ca::ViewPoint& vp =  global_vp_list_[i];
        Eigen::Vector3d & v = vp.vp_pose_.position_;
        p.x = v.x(); p.y = v.y(); p.z = v.z();
        m.points.push_back(p);
        if(i== gcb_.path_.path_[0] || i== gcb_.path_.path_[gcb_.path_.path_.size()-1])
            m.colors.push_back(cc);
        else
            m.colors.push_back(m.color);
    }
    ma.markers.push_back(m);
    if(mode<1) return ma;

    m.ns = "map";
    m.type = visualization_msgs::Marker::CUBE_LIST;
    m.scale.x = 5;
    m.scale.y = 5;
    m.scale.z = 0.1;
    m.color.a = 0.5; // Don't forget to set the alpha!
    m.color.r = 1.0;
    m.color.g = 1.0;
    m.color.b = 1.0;
    m.points.clear();
    m.colors.clear();
 //   geometry_msgs::Point p;
    for(size_t i=0; i < grid2_->dim_i(); i++){
        for(size_t j=0; j < grid2_->dim_j(); j++){
            std_msgs::ColorRGBA color;
            ca::FixedGrid2f::Vec2 vv = grid2_->grid_to_world(i,j);
            ca::SemanticGridCell& c = gcb_.path_.rep_state_[grid2_->grid_to_mem(i,j)];

            if(c.integrated_logodds_[(int)ca::SemanticClass::car]>0){
                color.a = std::min(((double)c.integrated_logodds_[(int)ca::SemanticClass::car])/100.0,1.0);
                color.r = 1.0; color.g = 0.0; color.b = 0.0;
            }
            else{
                color.a = std::min(-((double)c.integrated_logodds_[(int)ca::SemanticClass::car])/100.0,1.0);
                color.r = 0.0; color.g = 0.0; color.b = 0.0;
            }
            m.colors.push_back(color);
            p.x = vv.x(); p.y = vv.y(); p.z = c.height_mean_-c.class_properties_[(int)ca::SemanticClass::car].ReportHeight();
            m.points.push_back(p);
        }
    }
    ma.markers.push_back(m);
    return ma;
}

void ca::IPPWrapper::generate_global_vp_list(){

    ca::ViewPoint vp;
    float start_heading = 0;
    float start_fov = 2*M_PI;
    std::vector<Eigen::Vector2d> grid_points;
    ca::GetGrid(grid_points,p_ori_,p_x_,p_y_,x_res_,y_res_);
    Eigen::Vector3d v;
    for(size_t i=0;i<grid_points.size();i++)
    {
        v.x() = grid_points[i].x();
        v.y() = grid_points[i].y();
        v.z() = -grid_height_;
        vg_.MakeViewPoint(vp,v,start_heading,start_fov);
        global_vp_list_.push_back(vp);
    }

}

void ca::IPPWrapper::RewardTesting(){
    //add the start location to the
    /*gcb_.path_.path_.clear();
    ca::ViewPoint vp;
            vg_->MakeViewPoint(vp,start_point,start_heading,start_fov);
    global_vp_list_.push_back(vp);


    start_location_id_ = global_vp_list_.size()-1;
    path_.setRepresentationState(grid_data);
    path_.AddNode(start_location_id_);
    path_.AddNode(end_node_id);
    //ROS_ERROR_STREAM("List Size2::"<<global_vp_list_.size()<<" Path nodes::"<<path_.path_.size());
    size_t dummy;
    double distance, reward;
    double benefit, max_benefit;
    double budget_left = budget - path_.CalculatePathLength(path_.path_,global_vp_list_);
    bool found_admissible = true;

    while (budget_left>0 && found_admissible){
        //ROS_ERROR_STREAM("In while::"<<path_.path_.size());
        max_benefit = 0.0;
        size_t best_id =0;
        found_admissible = false;
        for(size_t i=0; i<global_vp_list_.size();i++){
            if(!path_.IsInPath(i,dummy)){
                distance =
                    path_.CostOfAdding(path_.path_,global_vp_list_,
                                       global_vp_list_[i].vp_pose_.position_);
                distance = std::max(0.1,distance);
                reward = reward_.RewardFunction(i,global_vp_list_,path_.rep_state_.data());*/

}

bool ca::IPPWrapper::Replan(Eigen::Vector3d lookahead_pose, double &budget){
    if(prev_lookahead_initialized_){
        budget_ = budget_ - (lookahead_pose-prev_lookahead_).norm();
    }
   prev_lookahead_ = lookahead_pose;
   prev_lookahead_initialized_ = true;
    budget = budget_;    
    if(published_traj_.waypoints.size()==0)
        return true;

    bool found = true;
    while(published_traj_.waypoints.size()>1 && found){
        geometry_msgs::Point p = published_traj_.waypoints[0].position;
        Eigen::Vector3d next_pos(p.x,p.y,p.z);
        found = false;
        if((next_pos-lookahead_pose).norm()<checkoff_distance_){
            found = true;
            published_traj_.waypoints.erase(published_traj_.waypoints.begin());
            if(published_traj_.waypoints.size()>0){
                if(published_traj_.waypoints[0].safety){
                    global_vp_list_.erase((global_vp_list_.begin()+gcb_.path_.path_[0]));
                }
            }
        }
    }
    if(published_traj_.waypoints.size()>0 && published_traj_.waypoints[0].safety){
        geometry_msgs::Point p = published_traj_.waypoints[0].position;
        Eigen::Vector3d next_pos(p.x,p.y,p.z);
        if((next_pos-lookahead_pose).norm()<replan_distance_)
            return true;
        else
            return false;
    }
    return false;
}

void ca::IPPWrapper::CleanUpVPList(){
    if(gcb_.path_.path_.size()>0){
        //gcb_.path_.path_[0]
        gcb_.path_.path_.clear();
        global_vp_list_.pop_back();
    }
}

visualization_msgs::MarkerArray ca::IPPWrapper::Displaytrajectory(ca_nav_msgs::PathXYZVViewPoint& traj){
    visualization_msgs::MarkerArray ma;
    visualization_msgs::Marker marker;
    marker.header.frame_id = "/world";
    marker.header.stamp = ros::Time();
    marker.ns = "vp_traj";
    marker.frame_locked = true;
    marker.id = 0;
    marker.type = visualization_msgs::Marker::LINE_STRIP;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0.0;
    marker.pose.position.y = 0.0;
    marker.pose.position.z = 0.0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 0.5;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color.a = 1.0;
    marker.color.r = 0.0;
    marker.color.g = 1.0;
    marker.color.b = 0.0;
    for(int i=0;i<traj.waypoints.size();i++){
        marker.points.push_back(traj.waypoints[i].position);
    }

    ma.markers.push_back(marker);
    marker.points.clear();

    marker.type = visualization_msgs::Marker::LINE_LIST;
    marker.id = 1;
    marker.scale.x = 0.2;
    marker.color.r = 0.0;
    marker.color.g = 0.0;
    marker.color.b = 1.0;

    for(int i=0;i<traj.waypoints.size();i++){
        if(!traj.waypoints[i].safety){
            marker.points.push_back(traj.waypoints[i].position);
            marker.points.push_back(traj.waypoints[i].viewpoint);
        }
    }
    ma.markers.push_back(marker);
    marker.points.clear();

    marker.type = visualization_msgs::Marker::SPHERE_LIST;
    marker.id = 2;
    marker.scale.x = 1.0;
    marker.color.r = 1.0;
    marker.color.g = 0.0;
    marker.color.b = 0.0;
    for(int i=0;i<traj.waypoints.size();i++){
        marker.points.push_back(traj.waypoints[i].position);
    }
    ma.markers.push_back(marker);
    return ma;
}

