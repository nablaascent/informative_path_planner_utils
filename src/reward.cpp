#include "informative_path_planner_utils/reward.h"

double ca::Reward::RewardFunction(size_t new_nodeId,std::vector<ViewPoint>& global_vp_list ,ca::SemanticGridCell *grid_data){
    ca::ViewPoint& vp = global_vp_list[new_nodeId];
    //ROS_INFO_STREAM("In reward function::"<<vp.visible_primitives_.size());
    double reward = 0;

    for(size_t i=0; i<vp.visible_primitives_.size();i++){
        ca::SemanticGridCell& sc = grid_data[vp.visible_primitives_[i]];
        int cell_p = std::abs(sc.integrated_logodds_[(size_t)ca::SemanticClass::car]);
        int abs_cell_p = std::abs(cell_p);
        if(abs_cell_p < max_p_ ){
            abs_cell_p = std::max(1,abs_cell_p);
            int num_rays = std::min(max_rays_, vp.num_rays_primitives_[i]);
            reward += std::pow(exp_base_,cell_p)*num_rays;
        }
    }
    return reward;
}
