#include "informative_path_planner_utils/gcb.h"


ca::GeneralizedCostBenefit::GeneralizedCostBenefit(std::vector<ViewPoint> & vp_list):
    path_(vp_list),
    global_vp_list_(vp_list)
{

};

void ca::GeneralizedCostBenefit::SetupAndSolveTsp(std::vector<size_t> &node_order, size_t start_location_id, size_t end_node_id){
    // start_location_id and end_location_id are global_vp_lists
    // setup the D matrix according to start and end
    int start_id, end_id, fake_id;

    for(int i=0; i<node_order.size();i++){
        if(node_order[i] == start_location_id)
            start_id = i;
        if(node_order[i] == end_node_id)
            end_id = i;
    }    
    fake_id = node_order.size();
    //ROS_INFO_STREAM("Setting up D!::"<<node_order.size()<<"::"<<start_id<<"::"<<end_id);
    Eigen::MatrixXd D = DMatrix(node_order, start_id, end_id);
    //ROS_INFO_STREAM("Initializing!");
    tsp_.Initialize(D);
    std::vector<size_t> new_order;
    //ROS_INFO_STREAM("Solving!");
    tsp_.Solve(new_order); //New Order is the index of node_order, so are start_id and end_id
    //ROS_INFO_STREAM("New Order--");
    //for(size_t i=0; i< new_order.size(); i++){
    //    ROS_INFO_STREAM(new_order[i]<<"-->");
    //}
    //ROS_INFO_STREAM("Fixing Order!");
    std::vector<size_t> new_order1 = FixOrder(new_order, start_id, end_id, fake_id);
    path_.ResetOrder(new_order1);

}

std::vector<size_t> ca::GeneralizedCostBenefit::FixOrder(std::vector<size_t> &order, int start_id, int end_id, int fake_id){
    int s_id,e_id, f_id;
    //ROS_INFO_STREAM("TSP order::start_id::end_id::fake_id::"<<start_id<<"::"<<end_id<<"::"<<fake_id);
    for(size_t i=0; i< order.size();i++)
    {
        //ROS_INFO_STREAM(order[i]<<"-->");
        if(order[i] == start_id)
            s_id = i;

        if(order[i] == end_id)
            e_id = i;

        if(order[i] == fake_id)
            f_id = i;

    }

    std::vector<size_t> new_order;
    if(f_id == 0 || f_id==(order.size()-1)){
        if(s_id < e_id){
            for(int i=s_id; i<= (int)e_id; i++){
                new_order.push_back(order[i]);
            }
        }
        else{
            for(int i=s_id; i>=(int)e_id; i--){
                new_order.push_back(order[i]);
            }
        }
    }
    else{

        if(s_id < e_id){
            for(int i=s_id; i>=0; i--){
                new_order.push_back(order[i]);
            }
            for(int i=order.size()-1; i>=e_id; i--){
                new_order.push_back(order[i]);
            }
        }
        else{
            for(int i=s_id; i<order.size(); i++){
                new_order.push_back(order[i]);
            }
            for(int i=0; i<=e_id; i++){
                new_order.push_back(order[i]);
            }
        }
    }

    return new_order;
}

Eigen::MatrixXd ca::GeneralizedCostBenefit::DMatrix(std::vector<size_t> &points, int start_id, int end_id){
    Eigen::MatrixXd D((points.size()+1),(points.size()+1));
    //ROS_INFO_STREAM("Allocated D");
    size_t fake_id = points.size();    
    double distance;
    Eigen::Vector3d p1,p2;
     for(size_t i=0; i<D.rows();i++){
        if(i<points.size()){
            ca::ViewPoint& vp = global_vp_list_[points[i]];
            p1 = vp.vp_pose_.position_;}
        for(size_t j=0; j<D.cols();j++){ // This can be
            if(i==fake_id || j==fake_id){
                D(i,j) = 1000000.0;
                D(j,i) = 1000000.0;
            }
            else{
                ca::ViewPoint& vp = global_vp_list_[points[j]];
                p2 = vp.vp_pose_.position_;
                distance = (p1-p2).norm();
                D(i,j) =distance;
            }
        }
        D(i,i) = 0;
    }

    D(fake_id,start_id) = 0.0000001;
    D(start_id,fake_id) = 0.0000001;
    D(fake_id,end_id) = 0.0000001;
    D(end_id,fake_id) = 0.0000001;
    return D;
}

ca::Path& ca::GeneralizedCostBenefit::GCB(Eigen::Vector3d& start_point, double start_heading, double start_fov,
                                     size_t end_node_id,
                                     ca::SemanticGridCell* grid_data,
                                     double budget){
    //add the start location to the
    path_.path_.clear();
    ca::ViewPoint vp;
            vg_->MakeViewPoint(vp,start_point,start_heading,start_fov);
    global_vp_list_.push_back(vp);


    start_location_id_ = global_vp_list_.size()-1;
    path_.setRepresentationState(grid_data);
    path_.AddNode(start_location_id_);
    path_.AddNode(end_node_id);
    //ROS_ERROR_STREAM("List Size2::"<<global_vp_list_.size()<<" Path nodes::"<<path_.path_.size());
    size_t dummy;
    double distance, reward;
    double benefit, max_benefit;
    double budget_left = budget - path_.CalculatePathLength(path_.path_,global_vp_list_);
    bool found_admissible = true;

    while (budget_left>0 && found_admissible){
        //ROS_ERROR_STREAM("In while::"<<path_.path_.size());
        max_benefit = 0.0;
        size_t best_id =0;
        found_admissible = false;
        for(size_t i=0; i<global_vp_list_.size();i++){
            if(!path_.IsInPath(i,dummy)){
                distance =
                    path_.CostOfAdding(path_.path_,global_vp_list_,
                                       global_vp_list_[i].vp_pose_.position_);
                distance = std::max(0.1,distance);
                reward = reward_.RewardFunction(i,global_vp_list_,path_.rep_state_.data());
                //ROS_ERROR_STREAM(i<<"::P::"<<global_vp_list_[i].vp_pose_.position_.x()<<
                //                 "::"<<global_vp_list_[i].vp_pose_.position_.y()<<"::"<<global_vp_list_[i].vp_pose_.position_.z());
                benefit = reward/distance;
                //ROS_ERROR_STREAM("R::"<<reward<<" Distance::"<<distance<<" Benefit::"<<benefit);
                if(benefit > max_benefit && distance < budget_left){
                    best_id = i;
                    found_admissible = true;
                    max_benefit = benefit;
                }

            }
        }
        //ROS_ERROR("Outside For:");
        if(found_admissible){
            path_.AddNode(best_id);
            //ROS_ERROR_STREAM("Added Node::"<<best_id);
            //ROS_ERROR_STREAM("About to solve TSP");
            SetupAndSolveTsp(path_.path_,start_location_id_,end_node_id);
            //ROS_INFO_STREAM("Solved TSP");
        }
    }

    /*for(size_t i=0; i<global_vp_list_.size();i++){
        ca::ViewPoint& vp =  global_vp_list_[i];
        Eigen::Vector3d & v = vp.vp_pose_.position_;
        ROS_INFO_STREAM("VP list3::"<<v.x()<<"::"<<v.y()<<"::"<<v.z());
    }*/


    return path_;
}
