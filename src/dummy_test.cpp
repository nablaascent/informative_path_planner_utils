#include "ros/ros.h"
#include "informative_path_planner_utils/ipp_wrapper.h"
#include "informative_path_planner_utils/grid_creator.h"
#include "informative_path_planner_utils/trajectory_maker.h"

visualization_msgs::MarkerArray disp_path(ca_nav_msgs::PathXYZVViewPoint& traj){
    visualization_msgs::MarkerArray ma;
    visualization_msgs::Marker marker;
    marker.header.frame_id = "/world";
    marker.header.stamp = ros::Time();
    marker.ns = "vp_path";
    marker.frame_locked = true;
    marker.id = 0;
    marker.type = visualization_msgs::Marker::LINE_STRIP;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0.0;
    marker.pose.position.y = 0.0;
    marker.pose.position.z = 0.0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 0.5;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color.a = 1.0;
    marker.color.r = 0.0;
    marker.color.g = 1.0;
    marker.color.b = 0.0;
    for(int i=0;i<traj.waypoints.size();i++){
        marker.points.push_back(traj.waypoints[i].position);
    }

    ma.markers.push_back(marker);
    marker.points.clear();

    marker.type = visualization_msgs::Marker::LINE_LIST;
    marker.id = 1;
    marker.scale.x = 0.2;
    marker.color.r = 0.0;
    marker.color.g = 0.0;
    marker.color.b = 1.0;

    for(int i=0;i<traj.waypoints.size();i++){
        if(!traj.waypoints[i].safety){
            marker.points.push_back(traj.waypoints[i].position);
            marker.points.push_back(traj.waypoints[i].viewpoint);
        }
    }
    ma.markers.push_back(marker);
    marker.points.clear();

    marker.type = visualization_msgs::Marker::SPHERE_LIST;
    marker.id = 2;
    marker.scale.x = 1.0;
    marker.color.r = 1.0;
    marker.color.g = 0.0;
    marker.color.b = 0.0;
    for(int i=0;i<traj.waypoints.size();i++){
        marker.points.push_back(traj.waypoints[i].position);
    }
    ma.markers.push_back(marker);
    return ma;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "dummy_test");
    ros::NodeHandle n;

    ros::Publisher ma_pub = n.advertise<visualization_msgs::MarkerArray>("/test_path/vis", 1);

    std::vector<ca::ViewPoint> vp;
    ca::IPPWrapper ipp(NULL, NULL,vp,std::string("world"));
    std::vector<Eigen::Vector2d> grid_points;
    Eigen::Vector2d p1(0,0);
    Eigen::Vector2d p2(400,0.0);
    Eigen::Vector2d p3(0,40);
    double x_res =40;
    double y_res = 30;
    ca::GetGrid(grid_points,p1,p2,p3,x_res,y_res);
    for(size_t i=0; i<grid_points.size();i++){
        std::cout<<grid_points[i].x()<<"::"<<grid_points[i].y()<<"\n";
    }

    std::vector<ca::ViewPoint> global_vp_list;
    Eigen::Vector3d curr_pos(-100,0,0);
    double curr_heading = 0;

    for(size_t i=0; i<grid_points.size();i++){
        ca::ViewPoint temp_vp;
        temp_vp.vp_pose_.position_ = Eigen::Vector3d(grid_points[i].x(),
                                     grid_points[i].y(),
                                     0.0);
        temp_vp.vp_pose_.heading_ = 0.0;
        temp_vp.vp_pose_.fov_ = M_2PI;
        global_vp_list.push_back(temp_vp);

    }


    ca::Path path(global_vp_list);

    for(size_t i=0; i<grid_points.size();i++){
        path.path_.push_back(i);
    }

    ca::TrajectoryGenerator tg;
    tg.vehicle_fov_ = M_PI/3;
    tg.time_bet_frames_ = 2.0;
    tg.high_velocity_ = 4;
    tg.low_velocity_ = 1;
    ca_nav_msgs::PathXYZVViewPoint traj = tg.MakeTrajectory(curr_pos, curr_heading,path, "/world");

    ros::Rate loop_rate(10);
    int count = 0;
    while (ros::ok())
    {
        //std::cout<<"Iter\n";
        visualization_msgs::MarkerArray ma =  disp_path(traj);
        ma_pub.publish(ma);
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
