#include "informative_path_planner_utils/grid_creator.h"

bool ca::GetGrid(std::vector<Eigen::Vector2d> &grid_points, Eigen::Vector2d p1, Eigen::Vector2d p2, Eigen::Vector2d p3, double x_res, double y_res){
    double x_size = (p2-p1).norm();
    double y_size = (p3-p1).norm();
    Eigen::Vector2d x_dir = p2-p1;
    x_dir.normalize();

    Eigen::Vector2d y_dir = p3-p1;
    y_dir.normalize();

    for(double xx=0; xx <= x_size; xx+=x_res){
        for(double yy=0; yy <= y_size; yy+=y_res){
            Eigen::Vector2d v;
            v = p1 + x_dir*xx;
            v = v + y_dir*yy;
            grid_points.push_back(v);
        }
    }

    return true;
}
